import os
import cStringIO
from flask import Flask
from flask import render_template, jsonify, request
from PIL import Image
import requests
from fingerprint import CNNFingerPrint
from werkzeug.utils import secure_filename

app = Flask(__name__)
model = CNNFingerPrint()

# upload directory
UPLOAD_FOLDER = 'static'
# allowed extensions
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg'])
# number of matches shown
NUM_MATCHES = 10
# key: filename, value: fingerprint
fps = {}

if not os.path.isdir(UPLOAD_FOLDER):
    os.mkdir(UPLOAD_FOLDER)

@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')
    

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    matches = []
    if request.method == 'POST':
        # get image
	print request.files
	if 'localfile' in request.files:	
          img = request.files['localfile']
	  if img and img.filename:
	    filename = secure_filename(img.filename)
	    img.save(filename)
            fp = model.get_image_features(filename)
            matches = model.find_closest_matches(fp, NUM_MATCHES)
 	else:
   	  print 'Requesting image...'
          img_url = request.form['image_url']
          img_resp = requests.get(img_url)
          img = Image.open(cStringIO.StringIO(img_resp.content))
 	  print 'Saving image...'
          img.save("tmp.png", "PNG")
          fp = model.get_image_features("tmp.png")
          matches = model.find_closest_matches(fp, NUM_MATCHES)
    return render_template('upload.html', filename="tmp.png", matches=matches,
                           upload_dir=UPLOAD_FOLDER)

if __name__ == '__main__':
  model.init_fps_store(UPLOAD_FOLDER, ALLOWED_EXTENSIONS)
  app.run(debug=True,
	  host=os.getenv('LISTEN', '0.0.0.0'),
          port=int(os.getenv('PORT', '5000')))

