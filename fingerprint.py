from keras.models import Sequential
from keras.layers.core import Flatten, Dense, Dropout
from keras.preprocessing import image
from keras.applications.vgg16 import preprocess_input
from keras.layers.convolutional import Convolution2D, MaxPooling2D, ZeroPadding2D
 
import numpy as np
from annoy import AnnoyIndex
import random
import os

class CNNFingerPrint:
  def __init__(self):
    self.CNN_weights_file_name = 'weights/vgg16_weights.h5'
    self.names = []
    self.dim = 4096

  def init_fps_store(self, dir_name, allowed_extension):
    print 'Building CNN model...'
    self.model = self.VGG_16(weights_path=self.CNN_weights_file_name)

    print 'Adding images to fingerprint stores...'
    ct = 0
    t = AnnoyIndex(self.dim)
    for dir_path, _, filenames in os.walk(dir_name):
      for filename in filenames:
        if '.' in filename and filename.rsplit('.', 1)[1] in allowed_extension:
	    img_path = os.path.join(dir_path, filename)
	    features = self.get_image_features(img_path)
	    t.add_item(ct, features[0])
	    self.names.append(img_path)
	    ct += 1
    t.build(10)
    t.save('fps_store.ann')
    
  def get_image_features(self, image_file_name):
    ''' Runs the given image_file to VGG 16 model and returns the 
    weights (filters) as a 1, 4096 dimension vector '''
    print 'Getting image features...'

    img = image.load_img(image_file_name, target_size=(224, 224))
    x = image.img_to_array(img)
    x = np.expand_dims(x, axis=0)
    x = preprocess_input(x)
    image_features = self.model.predict(x)
    image_features = image_features / np.linalg.norm(image_features)
    return image_features

  def find_closest_matches(self, image_features, num_matches=3):
    print 'Finding closest matches...'
    u = AnnoyIndex(self.dim)
    u.load('fps_store.ann') # super fast, will just mmap the file
    neighbors, dists = u.get_nns_by_vector(image_features[0], num_matches, include_distances=True)

    matches = []
    for idx, dist in zip(neighbors, dists):
      matches.append({'filename' : self.names[idx], 'username' : 'test_user1', 'distance' : dist})
    return matches


  def pop(self, model):
    if not model.outputs:
        raise Exception('Sequential model cannot be popped: model is empty.')
    else:
        model.layers.pop()
        if not model.layers:
            model.outputs = []
            model.inbound_nodes = []
            model.outbound_nodes = []
        else:
            model.layers[-1].outbound_nodes = []
            model.outputs = [model.layers[-1].output]
        model.built = False

    return model

  def VGG_16(self, weights_path=None):
    model = Sequential()
    model.add(ZeroPadding2D((1,1),input_shape=(3,224,224)))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(64, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(128, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(256, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(ZeroPadding2D((1,1)))
    model.add(Convolution2D(512, 3, 3, activation='relu'))
    model.add(MaxPooling2D((2,2), strides=(2,2)))

    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(4096, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(1000, activation='softmax'))

    if weights_path:
        model.load_weights(weights_path)

    #Remove the last two layers to get the 4096D activations
    model = self.pop(model)
    model = self.pop(model)
        
    return model


